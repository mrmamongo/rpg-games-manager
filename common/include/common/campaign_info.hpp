#ifndef RPGGAMEMANAGER_CAMPAIGN_INFO_HPP
#define RPGGAMEMANAGER_CAMPAIGN_INFO_HPP

#include <string>

namespace common {

struct CampaignInfo {
    std::string campaignName;
    std::string sessionName;
    size_t session;
    std::string description;
    bool registrationOpen;
    bool oneshot;
};

}  // namespace common

#endif //RPGGAMEMANAGER_CAMPAIGN_INFO_HPP
