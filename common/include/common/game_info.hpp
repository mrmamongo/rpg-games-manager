#ifndef RPGGAMEMANAGER_GAME_INFO_HPP
#define RPGGAMEMANAGER_GAME_INFO_HPP

#include <common/player_info.hpp>
#include <common/system_info.hpp>
#include <common/campaign_info.hpp>

#include <nlohmann/json_fwd.hpp>

#include <vector>
#include <cstddef>

namespace common {

class GameInfo {
public:
    GameInfo(SystemInfo systemInfo, CampaignInfo campaignInfo, PlayerInfo gm, size_t slots);

    [[nodiscard]] const SystemInfo& GetSystemInfo() const;
    [[nodiscard]] const CampaignInfo& GetCampaignInfo() const;
    [[nodiscard]] const PlayerInfo& GetGM() const;

private:
    SystemInfo m_systemInfo;
    CampaignInfo m_campaignInfo;
    PlayerInfo m_gm;

    std::vector<PlayerInfo> m_players;
    size_t m_totalSlots;
    size_t m_freeSlots;
};

}  // namespace common

#endif //RPGGAMEMANAGER_GAME_INFO_HPP
