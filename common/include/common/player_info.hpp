#ifndef RPGGAMEMANAGER_PLAYER_INFO_HPP
#define RPGGAMEMANAGER_PLAYER_INFO_HPP

#include <common/social_network.hpp>

#include <string>

namespace common {

struct PlayerInfo {
    std::string displayName;
    SocialNetwork socialNetwork;
    std::string id;
    std::string additionalInfo;
};

}  // namespace common

#endif //RPGGAMEMANAGER_PLAYER_INFO_HPP
