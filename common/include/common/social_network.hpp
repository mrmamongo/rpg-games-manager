#ifndef RPGGAMEMANAGER_SOCIAL_NETWORK_HPP
#define RPGGAMEMANAGER_SOCIAL_NETWORK_HPP

namespace common {

enum class SocialNetwork {
    VKontakte,
    Telegramm,
    Discord
};

}  // namespace common

#endif //RPGGAMEMANAGER_SOCIAL_NETWORK_HPP
