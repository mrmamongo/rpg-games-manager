#ifndef RPGGAMEMANAGER_SYSTEM_INFO_HPP
#define RPGGAMEMANAGER_SYSTEM_INFO_HPP

#include <string>

namespace common {

struct SystemInfo {
    std::string shortName;
    std::string fullName;
};

}  // namespace common

#endif //RPGGAMEMANAGER_SYSTEM_INFO_HPP
