#include <common/game_info.hpp>

namespace common {

GameInfo::GameInfo(SystemInfo systemInfo, CampaignInfo campaignInfo, PlayerInfo gm, size_t slots)
    : m_systemInfo(std::move(systemInfo))
    , m_campaignInfo(std::move(campaignInfo))
    , m_gm(std::move(gm))
    , m_players()
    , m_totalSlots(slots)
    , m_freeSlots(slots)
{}

const SystemInfo &GameInfo::GetSystemInfo() const {
    return m_systemInfo;
}

const CampaignInfo &GameInfo::GetCampaignInfo() const {
    return m_campaignInfo;
}

const PlayerInfo &GameInfo::GetGM() const {
    return m_gm;
}

}  // namespace common
