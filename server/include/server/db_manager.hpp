#ifndef RPGGAMEMANAGER_DB_MANAGER_HPP
#define RPGGAMEMANAGER_DB_MANAGER_HPP

#include <common/game_info.hpp>

#include <string>

#include <sqlite3.h>

namespace server {

class DBManager {
public:
    explicit DBManager(const std::string& filename);
    ~DBManager();

    bool AddNewGame(const common::GameInfo& gameInfo);

private:
    static std::pair<bool, bool> VerifySchema(sqlite3* db);
    static int PerformQueryNoResult(sqlite3* db, const char* query, int querySize);
    static void RebuildDatabase(sqlite3* db, bool drop = true);

private:
    sqlite3* m_db;
};

}  // namespace common

#endif //RPGGAMEMANAGER_DB_MANAGER_HPP
