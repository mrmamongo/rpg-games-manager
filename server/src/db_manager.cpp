#include <server/db_manager.hpp>

#include <filesystem>
#include <sstream>
#include <map>

namespace server {

DBManager::DBManager(const std::string& filename)
    : m_db(nullptr) {
    if (sqlite3_open(filename.c_str(), &m_db) != SQLITE_OK) {
        throw std::runtime_error("Database open/creation failed!");
    }

    auto [shouldRebuild, shouldDropTable] = VerifySchema(m_db);
    if (shouldRebuild) {
        RebuildDatabase(m_db, shouldDropTable);
    }
}

DBManager::~DBManager() {
    sqlite3_close(m_db);
}

std::pair<bool, bool> DBManager::VerifySchema(sqlite3 *db) {
    return {true, false};
}

int DBManager::PerformQueryNoResult(sqlite3 *db, const char *query, int querySize) {
    sqlite3_stmt* pStmt = nullptr;
    int rc = sqlite3_prepare_v2(db, query, querySize, &pStmt, nullptr);
    if (rc != SQLITE_OK) return rc;

    rc = sqlite3_step(pStmt);
    if (rc != SQLITE_DONE) return rc;

    rc = sqlite3_finalize(pStmt);
    return rc;
}

void DBManager::RebuildDatabase(sqlite3 *db, bool drop) {
    constexpr char dropTable[] = "DROP TABLE games";
    constexpr char createTable[] =
R"(CREATE TABLE games(
    GameID INTEGER PRIMARY KEY AUTOINCREMENT,
    SystemShort TEXT,
    SystemFull TEXT,

    CampaignName TEXT,
    SessionName TEXT,
    SessionNumber INTEGER,
    Description TEXT,
    RegistrationOpen INTEGER,
    Oneshot INTEGER,

    GMDisplayName TEXT,
    GMID TEXT,
    GMSocialNetwork TEXT,

    PlayerDisplayNames TEXT,
    PlayerIDs TEXT,
    PlayerAdditionalInfo TEXT,
    PlayerSocialNetworks TEXT,

    TotalSlots INTEGER,
    FreeSlots INTEGER,

    LastModified INTEGER
);)";
    int rc = SQLITE_OK;

    if (drop) {
        rc = PerformQueryNoResult(db, dropTable, std::size(dropTable));
        if (rc != SQLITE_OK) throw std::runtime_error("Failed dropping old table");
    }

    rc = PerformQueryNoResult(db, createTable, std::size(createTable));
    if (rc != SQLITE_OK) throw std::runtime_error("Failed initializing table");
}

bool DBManager::AddNewGame(const common::GameInfo& gameInfo) {
    constexpr std::string_view requestStart =
"INSERT INTO games(SystemShort, SystemFull, CampaignName, SessionName, SessionNumber, Description, RegistrationOpen, Oneshot, GMDisplayName, GMID, GMSocialNetwork, PlayerDisplayNames, PlayerIDs, PlayerAdditionalInfo, PlayerSocialNetworks, TotalSlots, FreeSlots, LastModified)\n\
VALUES (";
    std::stringstream ss;
    ss << requestStart << gameInfo.GetSystemInfo().shortName << ", " << gameInfo.GetSystemInfo().fullName
       << gameInfo.GetCampaignInfo().campaignName << ", " << gameInfo.GetCampaignInfo().sessionName << ", "
       << gameInfo.GetCampaignInfo().session << ", " << gameInfo.GetCampaignInfo().description << ", "
       << gameInfo.GetCampaignInfo().registrationOpen << ", " << gameInfo.GetCampaignInfo().oneshot << ", "
       << gameInfo.GetGM().displayName << gameInfo.Get

    return false;
}

}  // namespace server
