#include <iostream>

#include <server/db_manager.hpp>

int main() {
    std::cout << "Hello, World!" << std::endl;
    server::DBManager db("test.db");
    return 0;
}
